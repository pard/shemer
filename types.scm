; #################
; Simple Data Types
; #################

; Boolean
(boolean? #t) ; => #t
(boolean? #f) ; => #t
(not #f) ; => #t
(boolean? "String") ; => #f
(not "String") ; => #f, all non-#f statements are true

; Numbers
(number? 16) ; => #t
(number? 16.0) ; => #t
(number? 1.6e1) ; => #t
(number? 32/2) ; => #t
(number? 2+3i) ; => #t

(integer? 16) ; => #t
(integer? 16.0) ; => #f

(complex? 16) ; => #f
(complex? 2+3i) ; => #t

(real? 16) ; => #t
(real? 16.0) ; => #t

(eqv? 16 16) ; => #t
(eqv? 16 16.0) ; => #f

(= 16 16.0) ; => #t
(= 16 1.6e1) ; => #t
(= 16 #b10000) ; => #t
(= 16 #o20) ; => #t
(= 16 #x10) ; => #t

(< 3 2) ; => #t
(> 3 2) ; => #f

(+ 3 2) ; => 5
(- 3 2) ; => 1
(/ 3 2) ; => 3/2
(/ 6 2) ; => 3
(* 3 2) ; => 6
(expt 3 2) ; => 9

(- 3) ; => -3
(/ 3) ; => 1/3

(max 1 2 3 4 5) ; => 5
(min 1 2 3 4 5) ; => 1

(abs 3) ; => 3
(abs -3) ; => 3

(round 3.499999) ; => 3.0
(round 3.5) ; => 4.0
(floor 3.5) ; => 3.0
(ceiling 3.499999) ; => 3.0

; Chars
(char? #\a) ; => #t
(char? a) ; => #f

(char=? #\a #\a) ; => #t
(char<? #\a #\b) ; => #t
(char>=? #\a #\b) ; => #f

(char-ci=? #\a #\A) ; => #t
(char-ci<? #\a #\B) ; => #t

(char-downcase #\A) ; => #\a
(char-upcase #\a) ; => #\A

; Symbols
(quote xyz) ; xyz
'xyz ; xyz
(eqv? 'xyz (quote xyz)) ; => #t

(symbol? 'xyz) ; => #t
(symbol? 120) ; => #f

(eqv? XYZ xyz) ; => #t

(define xyz 10) ; xyz => 10
(set! xyz 9) ; xyz => 9

; ###################
; Compound Data Types
; ###################

; Strings
"This is a string"

(string #\s#\t#\r#\i#\n#\g) ; => "string"

(string-ref "string" 0) ; => #\s

(string-append "s" "t" "r" "i" "n" "g") ; => "string"

(string-set! (string #\s#\t#\r#\i#\n#\g) 3 #\a) ; => "strang"

; Vectors
(vector 1 2 3) ; => #(1 2 3)

(make-vector 3) ; => #(0 0 0)

(vector? (vector 1 2 3)) ; => #t

(vector-ref (vector 1 2 3) 0) ; => 1

(vector-set! (vector 1 2 3) 0 5) ; => #(5 2 3)

; Dotted pairs and lists
(cons 1 #\A) ; => (1 . #\A)
'(1 . #\A) ; => (1 . #\A)
(1 . #\A) ; => Error

(car '(1 . #\A)) ; => 1
(cdr '(1 . #\A)) ; => #\A

(set-car! '(1 . #\A) 2) ; => (2 . #\A)
(set-cdr! '(1 . #\A) #\B) ; => (1 . #\B)

(cons 1 (cons 2 (cons 3 4))) ; => (1 2 3 . 4)

(cons 1 (cons 2 (cons 3 '()))) ; => (1 2 3)
(list 1 2 3) ; => (1 2 3)
'(1 2 3) ; => (1 2 3)

'() ; => ()

(list-ref '(1 2 3) 2) ; => 3
(list-tail '(1 2 3) 1) ; => (2 3)

(pair? '(1 . 2)) ; => #t
(pair? '(1 2)) ; => #t
(pair? '(1 2 3)) ; => #t
(pair? '()) ; => #t

(list? '(1 2)) ; => #t
(list? '(1 . 2)) ; => #f
(list? '()) ; => #t

(null? '()) ; => #t
(null? '(1 . 2)) ; => #f
(null? '(1 2 3)) ; => #f

; Type conversion

(char->integer #\a) ; => 97
(integer->char 98) ; => #\b

(string->list "string") ; => (#\s #\t #\r #\i #\n #\g)
(list->string '(#\s #\t #\r #\i #\n #\g)) ; => "string"

(number->string 10) ; => "10"
(string->number "10") ; => 10
(string->number "10" 16) ; => 16
(string->number "10" 8) ; => 14

