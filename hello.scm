(define (println x)
  (display x)
  (newline))

(begin
  (println "Hello, world!"))
