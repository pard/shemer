# Schemer

Me learning scheme.

## Resources

- [SICP](https://mitpress.mit.edu/sites/default/files/sicp/index.html)
- [Teach Yourself Scheme in Fixnum Days](https://ds26gte.github.io/tyscheme/index-Z-H-1.html)
- The Little Scheme
